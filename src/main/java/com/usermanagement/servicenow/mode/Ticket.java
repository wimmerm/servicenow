package com.usermanagement.servicenow.mode;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "service_tickets")
public class Ticket {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @ApiModelProperty(notes = "Ticket Id")
    private int id;
    @ApiModelProperty(notes = "Ticket Name")
    private String name;
    @ApiModelProperty(notes = "Ticket Email")
    private String email;
    @Column(name = "id_person_creator")
    @ApiModelProperty(notes = "Ticket creator's Id")
    private int idPersonCreator;
    @Column(name = "id_person_assigned")
    @ApiModelProperty(notes = "Ticket assignee's Id")
    private int idPersonAssigned;
    @Column(name = "creation_datetime")
    @ApiModelProperty(notes = "Ticket creation Date & Time")
    private String creationDateTime;

    public Ticket() {
    }

    public Ticket(String name, String email, int idPersonCreator, int idPersonAssigned) {
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDateTime = LocalDateTime.now().toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(int idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public int getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(int idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public String getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(String creationDateTime) {
        this.creationDateTime = creationDateTime;
    }
}
