package com.usermanagement.servicenow;

import com.usermanagement.servicenow.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicenowApplication {


    public static void main(String[] args) {
        SpringApplication.run(ServicenowApplication.class, args);
    }

}
