package com.usermanagement.servicenow.dto;

import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

public class TicketDto {

    @ApiModelProperty(notes = "Ticket Id")
    private int id;
    @ApiModelProperty(notes = "Ticket Name")
    private String name;
    @ApiModelProperty(notes = "Ticket Email")
    private String email;
    @ApiModelProperty(notes = "Ticket creator's Id")
    private int idPersonCreator;
    @ApiModelProperty(notes = "Ticket assignee's Id")
    private int idPersonAssigned;
    @ApiModelProperty(notes = "Ticket creation Date & Time")
    private String creationDateTime;

    public TicketDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdPersonCreator() {
        return idPersonCreator;
    }

    public void setIdPersonCreator(int idPersonCreator) {
        this.idPersonCreator = idPersonCreator;
    }

    public int getIdPersonAssigned() {
        return idPersonAssigned;
    }

    public void setIdPersonAssigned(int idPersonAssigned) {
        this.idPersonAssigned = idPersonAssigned;
    }

    public String getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime() {
        this.creationDateTime =  LocalDateTime.now().toString();
    }
}
