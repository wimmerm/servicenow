package com.usermanagement.servicenow.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.Queue;

@Configuration
public class MessagesConfig {

    private final String URL = "tcp://localhost:61616";

    @Bean
    public Queue queue(){
        return new ActiveMQQueue("test");
    }

    @Bean
    public ActiveMQConnectionFactory factory(){
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
        factory.setBrokerURL(URL);
        return factory;
    }

    @Bean
    public JmsTemplate jmsTemplate(){
        return new JmsTemplate(factory());
    }
}
