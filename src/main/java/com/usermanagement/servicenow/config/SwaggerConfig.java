package com.usermanagement.servicenow.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket ticketApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.usermanagement.servicenow"))
                .paths(regex("/tickets.*"))
                .build()
                .apiInfo(ticketInfo());
    }

    private ApiInfo ticketInfo(){
        ApiInfo apiInfo = new ApiInfo(
                "Seminarka Service Now Swagger Dokumentace",
                "Swagger dokumentace dle zadani (snad...)",
                "1.0",
                "Terms of Service = null",
                new Contact("Marek Wimmer","test","marek.wimmer@gmail.com"),"test","www.test.test");
        return apiInfo;

    }

}


