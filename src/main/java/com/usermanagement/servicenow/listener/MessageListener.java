package com.usermanagement.servicenow.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class MessageListener {

    @JmsListener(destination = "test")
    public void listen(String message){
        System.out.println("Received this MQ message: " + message);
    }
}
