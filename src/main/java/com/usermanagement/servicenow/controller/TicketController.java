package com.usermanagement.servicenow.controller;


import com.usermanagement.servicenow.dto.TicketDto;
import com.usermanagement.servicenow.mode.Ticket;
import com.usermanagement.servicenow.service.TicketMapper;
import com.usermanagement.servicenow.service.TicketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

import javax.jms.Queue;
import java.util.Optional;

@RestController
@RequestMapping("/tickets/")
@Api(value = "Ticket Rest Endpoint")
public class TicketController {

    @Autowired
    @Qualifier("ticketService")
    TicketService ticketService;

    @Autowired
    TicketMapper ticketMapper;

    @Autowired
    @Qualifier("jmsTemplate")
    JmsTemplate jmsTemplate;

    @Autowired
    @Qualifier("queue")
    Queue queue;



    @ApiOperation(value = "Find Ticket by Id")
    @GetMapping("/find/{id}")
    public TicketDto findTicketId(@PathVariable("id") Integer id){

        Ticket ticketById = ticketService.findTicketById(id);
        return ticketMapper.mapToDto(ticketById);
    }

    @ApiOperation(value = "Find Ticket by Name")
    @GetMapping("/findBy/{name}")
    public TicketDto findTicketName(@PathVariable("name") String name){
        Ticket ticketByName = ticketService.findTicketByName(name);
        return ticketMapper.mapToDto(ticketByName);
    }

    @ApiOperation(value = "add new Ticket")
    @PostMapping("/add")
    @ResponseBody
    public TicketDto addTicket(@RequestBody Ticket ticket){
        jmsTemplate.convertAndSend(queue, "test Message");
        return ticketMapper.mapToDto(ticketService.addTicket(ticket));
    }

    @ApiOperation(value = "Find all Tickets Pagination style")
    @GetMapping("/all")
    public Page<Ticket> findAllTickets(@RequestParam Optional<String> name, @RequestParam Optional<Integer> page){
        return ticketService.findAllTickets(name,page);
    }

}
