package com.usermanagement.servicenow.service;

import com.usermanagement.servicenow.mode.Ticket;
import com.usermanagement.servicenow.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service
public class TicketService {

    @Autowired
    @Qualifier("ticketRepository")
    TicketRepository ticketRepository;

    @Transactional
    public Ticket findTicketById(int id){

        return ticketRepository.findAllById(id);
    }

    @Transactional
    public Ticket findTicketByName(String name){

        return ticketRepository.findAllByName(name);
    }

    @Transactional
    public Ticket addTicket(Ticket ticket){

        return ticketRepository.save(ticket);
    }

    @Transactional
    public Page<Ticket> findAllTickets(Optional<String> name, Optional<Integer> page){
        return ticketRepository.findAllByName(name.orElse(""),PageRequest.of(page.orElse(0),2) );
    }
}
