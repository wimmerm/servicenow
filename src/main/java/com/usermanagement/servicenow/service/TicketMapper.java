package com.usermanagement.servicenow.service;

import com.usermanagement.servicenow.dto.TicketDto;
import com.usermanagement.servicenow.mode.Ticket;
import org.mapstruct.Mapper;




@Mapper(componentModel = "spring")
public interface TicketMapper {

    TicketDto mapToDto(Ticket ticket);
    Ticket mapFromDto(TicketDto ticketDto);
}
