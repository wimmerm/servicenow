package com.usermanagement.servicenow.resource;

import com.usermanagement.servicenow.mode.Ticket;
import com.usermanagement.servicenow.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    TicketRepository ticketRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Ticket ticket1 = new Ticket();
        Ticket ticket2 = new Ticket();
        Ticket ticket3 = new Ticket();
        Ticket ticket4 = new Ticket();
        Ticket ticket5 = new Ticket();

        ticket1.setName("test1");
        ticket1.setEmail("test@test.cz");
        ticket1.setIdPersonAssigned(1);
        ticket1.setIdPersonCreator(4);
        ticket1.setCreationDateTime(LocalDateTime.now().toString());
        ticketRepository.save(ticket1);

        ticket2.setName("test2");
        ticket2.setEmail("test@test2.cz");
        ticket2.setIdPersonAssigned(3);
        ticket2.setIdPersonCreator(2);
        ticket2.setCreationDateTime(LocalDateTime.now().toString());
        ticketRepository.save(ticket2);

        ticket3.setName("test3");
        ticket3.setEmail("test@test3.cz");
        ticket3.setIdPersonAssigned(5);
        ticket3.setIdPersonCreator(3);
        ticket3.setCreationDateTime(LocalDateTime.now().toString());
        ticketRepository.save(ticket3);

        ticket4.setName("test4");
        ticket4.setEmail("test@test4.cz");
        ticket4.setIdPersonAssigned(7);
        ticket4.setIdPersonCreator(6);
        ticket4.setCreationDateTime(LocalDateTime.now().toString());
        ticketRepository.save(ticket4);

        ticket5.setName("test5");
        ticket5.setEmail("test@test5.cz");
        ticket5.setIdPersonAssigned(11);
        ticket5.setIdPersonCreator(14);
        ticket5.setCreationDateTime(LocalDateTime.now().toString());
        ticketRepository.save(ticket5);

    }
}
