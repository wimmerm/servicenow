package com.usermanagement.servicenow.repository;

import com.usermanagement.servicenow.mode.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface TicketRepository  extends JpaRepository<Ticket,Integer> {

    Ticket findAllById(Integer id);

    Ticket findAllByName(String ticketName);

    @Query("select s from Ticket s where name like %?1%")
    Page<Ticket> findAllByName(String name, Pageable pageable);

}
